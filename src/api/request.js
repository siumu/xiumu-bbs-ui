import axios from 'axios'
import { ElMessage } from 'element-plus'
import { getToken } from '@/utils/token'
import GlobalLoad from '@/utils/loading'

// create an axios instance
const service = axios.create({
  baseURL: 'http://localhost:8080', // url = base url + request url
  timeout: 5000 // request timeout
})

// request interceptor
service.interceptors.request.use(
  (config) => {
    // 设置请求头携带Token
    config.headers['XiumuToken'] = getToken()
    return config
  },
  (error) => {
    // do something with request error
    console.log(error) // for debug
    return Promise.reject(error)
  }
)

// response interceptor
service.interceptors.response.use(
  (response) => {
    console.log('response', response)
    // 处理返回值
    const res = response.data
    if (res.code !== 200) {
      GlobalLoad.hide()
      ElMessage.error(res.msg)
      return Promise.reject(new Error(res.msg || 'Error'))
    } else {
      return res
    }
  },
  (error) => {
    GlobalLoad.hide()
    ElMessage.error('网络错误！')
    return Promise.reject(error)
  }
)

/**
 * 封装请求方式
 * 方法命名规范：第一个单词是请求方式，第二个单词是请求参数类型
 */
export default class Http {
  /**
   * get请求, 传递 params 参数
   * @param url     请求地址
   * @param params  请求参数, 显示在 URL 上的参数
   * @param loading 是否打开全局加载 loading 页面
   */
  static getParams(url, params, loading = false) {
    return this.request(url, 'get', null, params, loading)
  }

  /**
   * get请求, 传递 data 参数
   * @param url     请求地址
   * @param data    请求 body, json格式，放在请求体传输
   * @param loading 是否打开全局加载 loading 页面
   */
  static getData(url, data, loading = false) {
    return this.request(url, 'get', data, null, loading)
  }

  /**
   * get请求, 传递 data 参数
   * @param url     请求地址
   * @param data    请求 body, json格式，放在请求体传输
   * @param params  请求参数, 显示在 URL 上的参数
   * @param loading 是否打开全局加载 loading 页面
   */
  static get(url, data, params, loading = false) {
    return this.request(url, 'get', data, params, loading)
  }

  /**
   * post请求
   * @param url     请求地址
   * @param params  请求参数, 显示在 URL 上的参数
   * @param loading 是否打开全局加载 loading 页面
   */
  static postParams(url, params, loading = false) {
    return this.request(url, 'post', null, params, loading)
  }

  /**
   * post请求
   * @param url     请求地址
   * @param data    请求 body, json格式，放在请求体传输
   * @param loading 是否打开全局加载 loading 页面
   */
  static postData(url, data, loading = false) {
    return this.request(url, 'post', data, null, loading)
  }

  /**
   * post请求
   * @param url     请求地址
   * @param data    请求 body, json格式，放在请求体传输
   * @param params  请求参数, 显示在 URL 上的参数
   * @param loading 是否打开全局加载 loading 页面
   */
  static post(url, data, params, loading = false) {
    return this.request(url, 'post', data, params, loading)
  }

  /**
   * put请求
   * @param url     请求地址
   * @param params  请求参数, 显示在 URL 上的参数
   * @param loading 是否打开全局加载 loading 页面
   */
  static putParams(url, params, loading = false) {
    return this.request(url, 'put', null, params, loading)
  }

  /**
   * put请求
   * @param url     请求地址
   * @param data    请求 body, json格式，放在请求体传输
   * @param loading 是否打开全局加载 loading 页面
   */
  static putData(url, data, loading = false) {
    return this.request(url, 'put', data, null, loading)
  }

  /**
   * put请求
   * @param url     请求地址
   * @param data    请求 body, json格式，放在请求体传输
   * @param params  请求参数, 显示在 URL 上的参数
   * @param loading 是否打开全局加载 loading 页面
   */
  static put(url, data, params, loading = false) {
    return this.request(url, 'put', data, params, loading)
  }

  /**
   * delete请求
   * @param url     请求地址
   * @param params  请求参数, 显示在 URL 上的参数
   * @param loading 是否打开全局加载 loading 页面
   */
  static deleteParams(url, params, loading = false) {
    return this.request(url, 'delete', null, params, loading)
  }

  /**
   * delete请求
   * @param url     请求地址
   * @param data    请求 body, json格式，放在请求体传输
   * @param loading 是否打开全局加载 loading 页面
   */
  static deleteData(url, data, loading = false) {
    return this.request(url, 'delete', data, null, loading)
  }

  /**
   * delete请求
   * @param url     请求地址
   * @param data    请求 body, json格式，放在请求体传输
   * @param params  请求参数, 显示在 URL 上的参数
   * @param loading 是否打开全局加载 loading 页面
   */
  static delete(url, data, params, loading = false) {
    return this.request(url, 'delete', data, params, loading)
  }

  /**
   * 请求
   * @param url     请求地址
   * @param method  请求方式 get, post, put, delete
   * @param data    请求 body, json格式，放在请求体传输
   * @param params  请求参数, 显示在 URL 上的参数
   * @param loading 是否打开全局加载 loading 页面
   */
  static request(url, method, data, params, loading = false) {
    if (loading) {
      GlobalLoad.show()
    }
    return service({
      url,
      method,
      data,
      params
    })
  }
}
