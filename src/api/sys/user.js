import Http from '../request'

export default class User {
  /**
   * 登录
   * @param username    账户
   * @param password    密码
   */
  static login(username, password) {
    return Http.postData('/user/login', { username, password }, true)
  }

  /**
   * 注册
   * @param userRegister  注册信息
   */
  static register(userRegister) {
    return Http.postData('/user/register', userRegister, true)
  }

  /**
   * 获取用户信息
   */
  static get() {
    return Http.getParams('/user/info', null, true)
  }

  /**
   * 退出登录
   */
  static logout() {
    return Http.getParams('/user/logout', null, true)
  }
}
