import { createApp } from 'vue'
import App from './App.vue'
import Element from 'element-plus'
import router from './router'
import store from '@/store'
import './routerGuard' // 路由守卫

import 'element-plus/dist/index.css'
import '@/style/index.css'

const app = createApp(App)
app.use(Element).use(router).use(store)

app.mount('#app')
