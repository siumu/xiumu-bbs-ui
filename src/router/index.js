import { createRouter, createWebHashHistory } from 'vue-router'

import Layout from '@/layout/index.vue'

/**
 * Tips:
 *
 * meta: {
 *   noLogin        免登录，true不需要登录，false需要登录；默认是false
 * }
 *
 */
export const constantRoutes = [
  {
    path: '/',
    component: Layout,
    redirect: '/dashboard',
    children: [
      {
        path: 'dashboard',
        component: () => import('@/views/dashboard/index.vue'),
        meta: { noLogin: true }
      },
      {
        path: 'publish-article',
        component: () => import('@/views/article/PublishArticle')
      }
    ]
  },
  {
    path: '/login',
    component: () => import('@/views/login/index.vue'),
    meta: { noLogin: true }
  },
  {
    path: '/article-info',
    component: () => import('@/views/article/index')
  }
]

const router = createRouter({
  history: createWebHashHistory(),
  routes: constantRoutes
})

export default router
