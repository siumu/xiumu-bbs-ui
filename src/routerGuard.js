/**
 * 路由守卫
 * 设置首页之外需要登录才可以进入
 */
import router from '@/router'
import NProgress from 'nprogress'
import 'nprogress/nprogress.css'
import { ElMessage } from 'element-plus'
import { getToken } from '@/utils/token'
import store from '@/store'

NProgress.configure({ showSpinner: false }) // NProgress Configuration

// 守卫规则
router.beforeEach(async (to, from, next) => {
  NProgress.start()
  console.log(to)
  // 检查是否有token，没有token再检查白名单放行
  const token = getToken()
  if (token) {
    // 如果有token就检查状态管理store中有没有userInfo
    // 如果没有userInfo，则重新请求一遍userInfo
    if (!store.getters.existUserInfo) {
      await store.dispatch('getUserInfo')
    }
    next()
  } else {
    // 检查是否免登录
    // 根据meta中的noLogin字段判断是否需要登录，
    if (to.meta.noLogin) {
      // in the free login whitelist, go directly
      next()
    } else {
      ElMessage.error('还未登录，请先登录！')
      next('/login')
    }
  }
})

router.afterEach(() => {
  // finish progress bar
  NProgress.done()
})
