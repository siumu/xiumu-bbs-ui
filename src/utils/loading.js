import { ElLoading } from 'element-plus'

// loading所需参数
const loadOption = {
  lock: true,
  text: 'Loading',
  spinner: 'el-icon-loading',
  background: 'rgba(0, 0, 0, 0.7)'
}

let loading = null
const show = () => {
  loading = ElLoading.service(loadOption)
}

const hide = () => {
  loading.close()
}

const GlobalLoad = {
  show,
  hide
}

export default GlobalLoad
