export const formRule = {
  required: [{ required: true, message: '该选项不可为空', trigger: 'blur' }],
  email: [{ type: 'email', required: true, message: '请输入正确的邮箱', trigger: 'change' }],
  phone: [{ required: true, pattern: /^1[3456789]\d{9}$/, message: '请输入正确的手机号', trigger: 'change' }]
}

/**
 * 获取 obj 的类型
 * [object object]
 * [object function]
 * [object string]
 * ...
 * @param obj
 */
function getType(obj) {
  return Object.prototype.toString.call(obj).toLowerCase()
}

/**
 * 判断 obj 是否为 Object 类型
 * @param obj
 */
export function isObject(obj) {
  return getType(obj) === '[object object]'
}

/**
 * 判断 obj 是否为 Function
 * @param obj
 */
export function isFunction(obj) {
  return getType(obj) === '[object function]'
}

/**
 * 判断 obj 是否为 Array 数组
 * @param obj
 */
export function isArray(obj) {
  return getType(obj) === '[object array]'
}

/**
 * 判断对象是否为空,
 * {}, null, undefined, '', [], 0 都返回 true
 * @param obj
 * @return {boolean}
 */
export function isEmpty(obj) {
  if (isObject(obj)) {
    return Object.keys(obj).length === 0
  } else if (isArray(obj)) {
    return obj.length === 0
  } else {
    return !obj
  }
}

/**
 * 判断对象不为空,
 * {}, null, undefined, '', [], 0 都返回 false
 * @param obj
 * @return {boolean}
 */
export function notEmpty(obj) {
  return !isEmpty(obj)
}
